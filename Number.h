//
//  Number.h
//  CommandLineTool
//
//  Created by Thomas Sanger Borthwick on 10/16/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef CommandLineTool_Number_h
#define CommandLineTool_Number_h


template <class Type>

class Number
{
public:
    Type get() const
    {
        std::cout << "yes value\n";
        return value;
    }
    
    void set(Type newValue)
    {
        value = newValue;
    }
    
private:
    Type value;
};


#endif
