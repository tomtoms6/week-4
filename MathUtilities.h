//
//  MathUtilities.h
//  CommandLineTool
//
//  Created by Thomas Sanger Borthwick on 10/16/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef __CommandLineTool__MathUtilities__
#define __CommandLineTool__MathUtilities__

#include <iostream>


class MathUtilities
{
public:

bool testUtil();

template <typename Type>
Type max (Type a, Type b);


template <typename Type>
Type min (Type a, Type b);


template <typename Type>
Type add (Type a,  Type b);


template <typename Type>
Type print (const Type a[], int b);


template <typename Type>
double getAverage (const Type a[], int b);

private:
    
};


#endif /* defined(__CommandLineTool__MathUtilities__) */
