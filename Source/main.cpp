//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "Array.h"
#include "Number.h"
#include "MathUtilities.h"

int main (int argc, const char* argv[])
{

    Array<int>  arrayInt1, arrayInt2;
    
    
    for (int i = 0; i < 10; i++)
    {
        arrayInt1.add(i);
        arrayInt2.add(i);
    }
    
    if (arrayInt1 != arrayInt2)
        std::cout << "Arrays are not same";
    else
        std::cout << "Arrays are same";
    
    arrayInt1[1] = 103;
    
    if (arrayInt1 != arrayInt2)
        std::cout << "Arrays are not same";
    else
        std::cout << "Arrays are same";
    
    return 0;
}







