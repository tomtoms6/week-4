//
//  MathUtilities.cpp
//  CommandLineTool
//
//  Created by Thomas Sanger Borthwick on 10/16/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#include "MathUtilities.h"


bool MathUtilities :: testUtil()
{
    bool test;
    int a = 1, b = 2;
    //int c[3] = {1,2,3};
    
    if (max(a,b) == b)
        test = 0;
    else
        return 1;
    
    if (min(a, b) == a)
        test = 0;
    else
        return 1;
    
    if (add(a, b) == a+b)
        test = 0;
    else
        return 1;
    
    return test;
}

template <typename Type>
Type MathUtilities :: max (Type a, Type b)
{
    if(a < b)
        return b;
    else
        return a;
}

template <typename Type>
Type MathUtilities :: min (Type a, Type b)
{
    if(a > b)
        return b;
    else
        return a;
}

template <typename Type>
Type MathUtilities :: add (Type a,  Type b)
{
    return a + b;
}

template <typename Type>
Type MathUtilities :: print (const Type a[], int b)
{
    for (int i = 0; i < b; i++)
        std::cout << a[i];
    
}

template <typename Type>
double MathUtilities :: getAverage (const Type a[], int b)
{
    double value = 0;
    
    for (int i = 0; i < b; i++)
        value += a[i];
    
    return value/b;
}
